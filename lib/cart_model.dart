
class Cart {

  late final int? id;
  final String? snackId;
  final String? snackName;
  final int? initialPrice;
  final int? snackPrice;
  final int? quantity;
  final String? unitTag;
  final String? image;

  Cart({
    required this.id,
    required this.snackId,
    required this.snackName,
    required this.initialPrice,
    required this.snackPrice,
    required this.quantity,
    required this.unitTag,
    required this.image
  });

  Cart.fromMap(Map<dynamic , dynamic> res)
      : id = res['id'],
        snackId = res["snackId"],
        snackName = res["snackName"],
        initialPrice = res["initialPrice"],
        snackPrice = res["snackPrice"],
        quantity = res["quantity"],
        unitTag = res["unitTag"],
        image = res["image"];

  Map<String, Object?> toMap(){
    return {
      'id' : id ,
      'productId' : snackId,
      'productName' :snackName,
      'initialPrice' : initialPrice,
      'productPrice' : snackPrice,
      'quantity' : quantity,
      'unitTag' : unitTag,
      'image' : image,
    };
  }


}
