
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

class SnackListScreen extends StatefulWidget {
  const SnackListScreen({Key? key}) : super(key: key);

  @override
  _SnackListScreenState createState() => _SnackListScreenState();
}

class _SnackListScreenState extends State<SnackListScreen> {

  List<String> sanckName = ['Potato chips' , 'Filled Cookies' , 'Chocolate Bar' , 'Candy' , 'Frozen Chocolate Cake' , 'Chewing gum','Wafer','Cookie','Cashews','Almonds',] ;
  List<String> sanckUnit = ['Slice' , 'Slice' , 'Pack' , 'Slice' , 'Slice' , 'Slice','Slice', 'Slice' , 'Slice' ,'Slice' ,] ;
  List<int> sanckPrice = [20, 20 , 25 , 45 , 110, 12 , 65 , 72 , 15, 25,] ;
  List<String> sanckImage = [
    'https://backend.tops.co.th/media/catalog/product/8/8/8850718801213_10-01-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/4/8/4893049150012_11-05-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/9/5/9556001071750_15-11-2021.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/0/0/0000096083864_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850231000582.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8852008610130_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/0/8000380157549_10-02-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850332251111_12_03-2021.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850291210464_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850096870092_e13-07-2020.jpg?impolicy=resize&height=300&width=300',
  ] ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product List'),
        centerTitle: true,
        actions: [
          Center(
            child: Badge(
              badgeContent: Text("0",style: TextStyle(color: Colors.white),),
              animationDuration: Duration(milliseconds: 300),
              child: Icon(Icons.shopping_basket_rounded),
            ),
          ),

          SizedBox(width: 20.0),

        ],
      ),
      body: Column(
        children: [
          Expanded(
              child: ListView.builder(
                  itemCount: sanckName.length,
                  itemBuilder: (context,index){
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Image(
                              height: 100,
                              width: 100,
                              image: NetworkImage(sanckImage[index].toString()),
                            ),
                            SizedBox(width: 10,),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(sanckName[index].toString(),
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(height: 5,),
                                  Text(sanckUnit[index].toString() + " " + r"$" + sanckPrice[index].toString(),
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(height: 5,),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Container(
                                      height: 35,
                                      width: 100,
                                      decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      child: Center(
                                        child: Text('Add to cart',style: TextStyle(color: Colors.white),),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }),
          ),
        ],
      ),
    );
  }
}

